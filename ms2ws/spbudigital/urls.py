from django.urls import path
from ms2ws.spbudigital import views

urlpatterns = [
    path('sales/', views.sales_list),
    path('totalisator/', views.totalisator),
    path('stock/', views.stock),
    path('penerimaan/', views.penerimaan),
    path('stock_kritis/', views.stock_kritis),
    #path('snippets/<int:pk>/', views.snippet_detail),
]