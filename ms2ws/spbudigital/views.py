from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User, Group

from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.decorators import api_view, authentication_classes, permission_classes

from rest_framework.parsers import JSONParser
from .models import Sales, Totalisator, Stock, Penerimaan, Stockkritis
from .serializers import SalesSerializer, UserSerializer, GroupSerializer, TotalisatorSerializer,StockSerializer,PenerimaanSerializer,StockkritisSerializer

from rest_framework import permissions
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


@api_view(['GET', 'POST'])
@authentication_classes([BasicAuthentication])
@permission_classes([IsAuthenticated])
def sales_list(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        sales = Sales.objects.all()
        serialz = SalesSerializer(sales, many=True)
        return JsonResponse(serialz.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        #print(request.data)
        serializer = SalesSerializer(data=data, many=True)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201,safe=False)
        return JsonResponse(serializer.errors, status=400)

@api_view(['GET', 'POST'])
@authentication_classes([BasicAuthentication])
@permission_classes([IsAuthenticated])
def totalisator(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        totalisator = Totalisator.objects.all()
        serialz = TotalisatorSerializer(totalisator, many=True)
        return JsonResponse(serialz.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = TotalisatorSerializer(data=data, many=True)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201,safe=False)
        return JsonResponse(serializer.errors, status=400)

@api_view(['GET', 'POST'])
@authentication_classes([BasicAuthentication])
@permission_classes([IsAuthenticated])
def stock(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        data = Stock.objects.all()
        serialz = StockSerializer(data, many=True)
        return JsonResponse(serialz.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = StockSerializer(data=data, many=True)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201,safe=False)
        return JsonResponse(serializer.errors, status=400)

@api_view(['GET', 'POST'])
@authentication_classes([BasicAuthentication])
@permission_classes([IsAuthenticated])
def penerimaan(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        data = Penerimaan.objects.all()
        serialz = PenerimaanSerializer(data, many=True)
        return JsonResponse(serialz.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = PenerimaanSerializer(data=data, many=True)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201,safe=False)
        return JsonResponse(serializer.errors, status=400)

@api_view(['GET', 'POST'])
@authentication_classes([BasicAuthentication])
@permission_classes([IsAuthenticated])
def stock_kritis(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        data = Stockkritis.objects.all()
        serialz = StockkritisSerializer(data, many=True)
        return JsonResponse(serialz.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = StockkritisSerializer(data=data , many=True)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201,safe=False)
        return JsonResponse(serializer.errors, status=400)