from django.db import models

# Create your models here.
class Sales(models.Model):
    push_datetime = models.DateTimeField(auto_now_add=False)
    #sales_datetime = models.DateTimeField(auto_now=False,)
    #added some time
    sales_time = models.TimeField(auto_now=False,blank=True,default='00:00:09',)
    sales_date = models.DateField(auto_now_add=False, default='1960-01-01',)
    sales_hour = models.SmallIntegerField(blank=True, default='0')
    #done
    no_spbu = models.CharField(max_length=100, blank=True, default='')
    dispenser = models.CharField(max_length=100, blank=True, default='')
    nozzle = models.CharField(max_length=100, blank=True, default='')
    
    produk = models.CharField(max_length=100, blank=True, default='')
    tipe_bayar = models.CharField(max_length=100, blank=True, default='')
    shift = models.CharField(max_length=100, blank=True, default='')
    qty_sales = models.CharField(max_length=100, blank=True, default='')
    revenue = models.CharField(max_length=100, blank=True, default='')

    class Meta:
        ordering = ['push_datetime']

class Totalisator(models.Model):
    push_datetime = models.DateTimeField(auto_now_add=False)
    this_datetime = models.DateTimeField(auto_now=False,)
    #totalistator time
    totalisator_time = models.TimeField(auto_now=False,blank=True,default='00:00:09')
    no_spbu = models.CharField(max_length=100, blank=True, default='')
    dispenser = models.CharField(max_length=100, blank=True, default='')
    nozzle = models.CharField(max_length=100, blank=True, default='')
    produk = models.CharField(max_length=100, blank=True, default='')
    vol_awal = models.CharField(max_length=100, blank=True, default='')
    vol_akhir = models.CharField(max_length=100, blank=True, default='')
    rev_awal = models.CharField(max_length=100, blank=True, default='')
    rev_akhir = models.CharField(max_length=100, blank=True, default='')

    class Meta:
        ordering = ['push_datetime']

class Stock(models.Model):
    push_datetime = models.DateTimeField(auto_now_add=False)
    stock_datetime = models.DateTimeField(auto_now=False,)
    no_spbu = models.CharField(max_length=100, blank=True, default='')
    produk = models.CharField(max_length=100, blank=True, default='')
    no_tanki = models.CharField(max_length=100, blank=True, default='')
    current = models.CharField(max_length=100, blank=True, default='')

    class Meta:
        ordering = ['push_datetime']

class Penerimaan(models.Model):
    push_datetime = models.DateTimeField(auto_now_add=False)
    penerimaan_datetime = models.DateTimeField(auto_now=False,)
    no_spbu = models.CharField(max_length=100, blank=True, default='')
    produk = models.CharField(max_length=100, blank=True, default='')
    no_tanki = models.CharField(max_length=100, blank=True, default='')
    qty_penerimaan = models.CharField(max_length=100, blank=True, default='')
    no_lo = models.CharField(max_length=100, blank=True, default='')

    class Meta:
        ordering = ['push_datetime']

class Stockkritis(models.Model):
    push_datetime = models.DateTimeField(auto_now_add=False)
    stockkritis_datetime = models.DateTimeField(auto_now=False,)
    no_spbu = models.CharField(max_length=100, blank=True, default='')
    no_tanki = models.CharField(max_length=100, blank=True, default='')
    produk = models.CharField(max_length=100, blank=True, default='')
    kapasitas_tangki = models.CharField(max_length=100, blank=True, default='')
    stok_saatini = models.CharField(max_length=100, blank=True, default='')
    persentase = models.CharField(max_length=100, blank=True, default='')

    class Meta:
        ordering = ['push_datetime']
