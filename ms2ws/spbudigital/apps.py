from django.apps import AppConfig


class SpbudigitalConfig(AppConfig):
    name = 'spbudigital'
