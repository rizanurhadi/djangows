from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import Sales, Totalisator, Stock, Penerimaan, Stockkritis

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']

class SalesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sales
        fields = ['no_spbu', 'nozzle','dispenser', 'produk', 'tipe_bayar', 'shift', 'qty_sales', 'push_datetime','sales_time','sales_date','sales_hour','revenue']

class TotalisatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Totalisator
        fields = ['no_spbu','dispenser', 'nozzle', 'produk', 'vol_awal', 'vol_akhir','rev_awal','rev_akhir', 'push_datetime','this_datetime','totalisator_time']

class StockSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stock
        fields = ['stock_datetime','no_spbu', 'produk', 'no_tanki', 'current', 'push_datetime']

class PenerimaanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Penerimaan
        fields = ['penerimaan_datetime','no_spbu', 'produk', 'no_tanki', 'qty_penerimaan','no_lo', 'push_datetime',]

class StockkritisSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stockkritis
        fields = ['stockkritis_datetime','no_spbu', 'produk', 'no_tanki', 'kapasitas_tangki','stok_saatini','persentase', 'push_datetime']
